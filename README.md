Redises
=======

redis distributed

Install
=======

Copy redises.php to your project library directory

Usage
=======

define('REDISES_DEBUG', true);<br />
require_once './redises.php';

$redis = new Redises();<br />
$redis->mode = REDIS_RAND;//rand server<br />
//or<br />
$redis = new Redises(REDIS_RAND);//default is REDIS_BALAN

$redis->addserver('192.168.0.182',6379);<br />
$redis->addserver('192.168.0.183',6379);<br />
$redis->addserver('192.168.0.185',6379);<br />
$r = $redis->rpush('queue_name','msg');


RedisQ
=======
$redis = new RedisQ();<br />
$redis->addserver('192.168.0.182');

$redis->push('queue_name', 'msg1');<br />
$redis->push('queue_name', 'msg2');

while ($redis->allConnected() && $redis->sumllen()){<br />
&nbsp;&nbsp;&nbsp;&nbsp;$msg = $redis->pop('queue_name');<br />
&nbsp;&nbsp;&nbsp;&nbsp;echo $msg,"\<br \/\>\n";<br />
}


RedisX
=======
$redis = new RedisX();<br />
$redis->addserver('192.168.0.182');

$r = $redis->set('key_name','value');<br />
$r = $redis->set('key_name',array('k','k'=>123));